import 'package:polymer/polymer.dart';

import 'dart:html';

@CustomTag('basics-poly')
class Basics extends PolymerElement with Observable {
  Element _dragSourceEl;

  Basics.created() : super.created();

  attached() {
    var col = $['column'];
    print('attached - $col');
    col.onDragStart.listen(_onDragStart);
    col.onDragEnd.listen(_onDragEnd);
    col.onDragEnter.listen(_onDragEnter);
    col.onDragOver.listen(_onDragOver);
    col.onDragLeave.listen(_onDragLeave);
    col.onDrop.listen(_onDrop);
  }

  void _onDragStart(MouseEvent event) {
    print('_onDragStart');
    Element dragTarget = event.target;
    dragTarget.classes.add('moving');
    _dragSourceEl = dragTarget;
    event.dataTransfer.effectAllowed = 'move';
    event.dataTransfer.setData('text/html', dragTarget.innerHtml);
  }

  void _onDragEnd(MouseEvent event) {
    print('_onDragEnd');
    Element dragTarget = event.target;
    dragTarget.classes.remove('moving');
    var cols = document.querySelectorAll('#columns .column');
    for (var col in cols) {
      col.classes.remove('over');
    }
  }

  void _onDragEnter(MouseEvent event) {
    print('_onDragEnter');
    Element dropTarget = event.target;
    dropTarget.classes.add('over');
    event.preventDefault();
    print(event.dataTransfer.getData('text/html'));
  }

  void _onDragOver(MouseEvent event) {
//    print('_onDragOver');
    // This is necessary to allow us to drop.
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }

  void _onDragLeave(MouseEvent event) {
    print('_onDragLeave');
    Element dropTarget = event.target;
    dropTarget.classes.remove('over');
  }

  void _onDrop(MouseEvent event) {
    print('_onDrop');
    // Stop the browser from redirecting.
    event.stopPropagation();

    // Don't do anything if dropping onto the same column we're dragging.
    Element dropTarget = event.target;
    if (_dragSourceEl != dropTarget) {
      // Set the source column's HTML to the HTML of the column we dropped on.
//      _dragSourceEl.innerHtml = dropTarget.innerHtml;
      print(event.dataTransfer.getData('text/html'));
    }
  }
}
