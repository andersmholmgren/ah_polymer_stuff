import 'package:polymer/polymer.dart';
import 'package:logging/logging.dart';

@CustomTag('test-row')
class TestRow extends PolymerElement with Observable {
//  @published ObservableList data;
//  @published double height;
//
//  dataChanged() {
//    print('dataChanged $data');
//  }

  TestRow.created() : super.created() {
    hierarchicalLoggingEnabled = true;
    Logger.root.level = Level.ALL;

    Logger.root.onRecord.listen((rec) {
      print('${rec.time.toIso8601String()} $rec');
    });
  }
}
