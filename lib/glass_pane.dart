library ui.polymer.core.glasspane;

import 'package:logging/logging.dart';
import 'package:polymer/polymer.dart';
import 'package:core_elements/core_overlay.dart';

Logger _log = new Logger('ui.polymer.core.glasspane');

@CustomTag('glass-pane')
class GlassPane extends PolymerElement with Observable {
  @published bool opened = false;

  CoreOverlay _overlay;

  GlassPane.created() : super.created();

  ready() {
    _overlay = $['overlay'];
    _overlay.target = this;
  }

  toggle() {
    _overlay.toggle();
  }

  openedChanged() {
    _log.finest('glasspane opened changed to "$opened"');
  }
}
