library ui.polymer.core.list.item.draggable;

import 'package:polymer/polymer.dart';
import 'dart:html';
import 'package:logging/logging.dart';
import 'package:core_elements/core_list_dart.dart';
import 'package:option/option.dart';
import 'package:ah_polymer_stuff/draggable_event.dart';

Logger _log = new Logger('ui.polymer.core.item.draggable');

@CustomTag('draggable-list-item')
class DraggableListItem extends PolymerElement with Observable {
  bool multi;
  CoreList _containingList;
  bool _isBeingDragged = false;
  int _originalIndex;
  int _enterCount = 0;
  Element _rowContainer;

  DraggableListItem.created() : super.created() {
    _log.finer('DraggableListItem.created');
  }

  void _clearDragStates() {
    _isBeingDragged = false;
    _enterCount = 0;
    _rowContainer.classes.remove('over');
    _rowContainer.classes.remove('moving');
  }

  ready() {
    _log.finest('DraggableListItem ready');
    _rowContainer = $['row-container'];

    _rowContainer.onDragStart.listen(_onDragStart);
    _rowContainer.onDragEnter.listen(_onDragEnter);
    _rowContainer.onDragOver.listen(_onDragOver);
    _rowContainer.onDragLeave.listen(_onDragLeave);
    _rowContainer.onDragEnd.listen(_onDragEnd);
    _rowContainer.onDrop.listen(_onDrop);

    super.ready();
  }

  attached() {
    _log.finest('DraggableListItem attached');
    _containingList = parentNode;
    super.attached();
  }

  void _onDragStart(MouseEvent event) {
    _log.finest('_onDragStart');

    Element dragTarget = event.target;
    _getContainingItem(dragTarget).map((dragItem) {
      dragItem._rowContainer.classes.add('moving');
      event.dataTransfer.effectAllowed = 'move';

      dragItem._isBeingDragged = true;
      dragItem._originalIndex = _listIndex(dragItem);
    });
  }

  void _onDragEnter(MouseEvent event) {
    _log.finest('_onDragEnter');
    event.preventDefault();
    Element dropTarget = event.target;

    final underItemOpt = _getContainingItem(dropTarget);
    underItemOpt.map((underItem) {
      underItem._enterCount++;
      underItem._rowContainer.classes.add('over');
    });
  }

  void _onDragOver(MouseEvent event) {
//    _log.finest('_onDragOver');
    // This is necessary to allow us to drop.
    event.preventDefault();

    event.dataTransfer.dropEffect = 'move';
  }

  void _onDragLeave(MouseEvent event) {
    _log.finest('_onDragLeave');
    // This is necessary to allow us to drop.
    event.preventDefault();

    Element dropTarget = event.target;

    final underItemOpt = _getContainingItem(dropTarget);
    underItemOpt.map((underItem) {
      underItem._enterCount--;
//      print('-------- _enterCount: ${underItem._enterCount}');
      // the _enterCount is to workaround the enter / leave events that occur
      // when they pointer is over text. No idea why that generates events
      if (underItem._enterCount == 0) {
        underItem._rowContainer.classes.remove('over');
      }
    });
  }

  _onDrop(MouseEvent event) {
    _log.finest('_onDrop');
    Element dropTarget = event.target;

    _draggedItem.map((DraggableListItem draggedItem) {
      final underItemOpt = _getContainingItem(dropTarget);

      underItemOpt.map((underItem) {
        _log.finest('underItem $underItem (${identityHashCode(underItem)})');
        final fromIndex = draggedItem._originalIndex;
        final toIndex = _listIndex(underItem);

        draggedItem._isBeingDragged = false;
        draggedItem._originalIndex = -1;
        draggedItem._rowContainer.classes.remove('moving');

        underItem._rowContainer.classes.remove('over');

        final moveEvent = new CoreListItemMoved(fromIndex, toIndex);

        _log.finest(moveEvent);

        fire('list-item-moved', detail: moveEvent);
      });
    });
  }

  void _onDragEnd(MouseEvent event) {
    _log.finest('_onDragEnd');

    _draggedItem.map((DraggableListItem draggedItem) {
      _log.finest('dropped off target');

      final originalIndex = draggedItem._originalIndex;
      final currentIndex = _listIndex(draggedItem);
      _log.finest('orig $originalIndex cur $currentIndex');

      draggedItem._isBeingDragged = false;
      draggedItem._originalIndex = -1;
      draggedItem._rowContainer.classes.remove('moving');

      if (currentIndex == originalIndex) {
        // nothing to move
      } else if (currentIndex < originalIndex) {
//        final insertIndex = originalIndex + 1;
//        if (insertIndex == _listMembers.length) {
//          _containingList.append(draggedItem);
//        } else {
//          _containingList.insertBefore(draggedItem, _listMembers[insertIndex]);
//        }
      } else {
//        _containingList.insertBefore(draggedItem, _listMembers[originalIndex]);
      }
    });

    _listMembers.forEach((draggableItem) {
      draggableItem._clearDragStates();
    });
  }

  Option<DraggableListItem> _getContainingItem(Node node) {
    return new Option(node).flatMap((dt) {
      if (dt is DraggableListItem) {
        return new Some(dt);
      }
      if (dt is ShadowRoot) {
        return _getContainingItem(dt.host);
      }
      return _getContainingItem(node.parentNode);
    });
  }

  Option<DraggableListItem> get _draggedItem => new Option(
      _listMembers.firstWhere((e) => e._isBeingDragged, orElse: () => null));

  int _listIndex(DraggableListItem item) => _listMembers.indexOf(item);

  List<DraggableListItem> get _listMembers => _containingList.children
      .where((e) => e is DraggableListItem)
      .map((DraggableListItem e) => e)
      .toList(growable: false);
}
