library ui.polymer.core.list.item.draggable.event;

class CoreListItemMoved {
  final int fromIndex;
  final int toIndex;

  CoreListItemMoved(this.fromIndex, this.toIndex);

  String toString() => 'list item moved from index $fromIndex to $toIndex';
}
