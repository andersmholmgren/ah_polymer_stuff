library ui.polymer.core.dragdrop;

import 'package:polymer/polymer.dart';
import 'dart:html';
import 'package:logging/logging.dart';

Logger _log = new Logger('ui.polymer.core.item.draggable');

@CustomTag('core-drag-drop')
class CoreDragDrop extends PolymerElement with Observable {
  var x, y;
  var avatar;
  bool dragging;
  Map dragInfo;

  CoreDragDrop.created() : super.created();

//    observe: {
//      'x y': 'coordinatesChanged'
//    },

  ready() {
    print('ready');
    if (avatar != null) {
      avatar = document.createElement('core-drag-avatar');
      document.body.append(avatar);
    }
    this.avatar = avatar;
    this.dragging = false;
  }

  draggingChanged() {
    print('dragChanged');
    this.avatar.style.display = this.dragging ? '' : 'none';
  }

  @ObserveProperty('x y')
  coordinatesChanged() {
    print('coordinatesChanged');
    var x = this.x,
        y = this.y;
    this.avatar.style.transform = this.avatar.style.webkitTransform =
        'translate(' + x + 'px, ' + y + 'px)';
  }

  attached() {
    print('attached');
//      var listen = (event, handler) {
//        parentNode.addEventListener(event, event, this[handler].bind(this));
//      }.bind(this);
    Element host =
        parentNode is ShadowRoot ? (parentNode as ShadowRoot).host : parentNode;

    print(host);
    print(host.getBoundingClientRect());

//      shadowRoot.onDragStart.listen((e) {
//        print('drag start');
//      });

    host.onDragStart.listen((e) {
      print('drag start');
    });

    listen(event, handler) {
      host.addEventListener(event, handler);
//        host.on
    }
    //
    listen('trackstart', trackStart);
    listen('track', track);
    listen('trackend', trackEnd);
    //
    host.style.cssText +=
        '; user-select: none; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none;';
  }

  trackStart(event) {
    print('trackStart');
    this.avatar.style.cssText = '';
    this.dragInfo = {'event': event, 'avatar': avatar};
    fire('drag-start', detail: dragInfo);
    // flaw #1: what if user doesn't need `drag()`?
    this.dragging = dragInfo['drag'] != null;
  }

  track(event) {
    print('track');
    if (this.dragging) {
      this.x = event.pageX;
      this.y = event.pageY;
      this.dragInfo['event'] = event;
      this.dragInfo['p'] = {'x': x, 'y': y};
      this.dragInfo['drag'](dragInfo);
    }
  }

  trackEnd(event) {
    print('trackEnd');
    if (this.dragging) {
      this.dragging = false;
      if (this.dragInfo['drop'] != null) {
        this.dragInfo['framed'] = this.framed(event.relatedTarget);
        this.dragInfo['event'] = event;
        this.dragInfo['drop'](this.dragInfo);
      }
    }
    this.dragInfo = null;
  }

  framed(node) {
    print('framed');
    var local = node.getBoundingClientRect();
    return {'x': this.x - local.left, 'y': this.y - local.top};
  }
}
