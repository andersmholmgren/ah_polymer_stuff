library ui.polymer.core.staticpage;

import 'package:logging/logging.dart';
import 'package:polymer/polymer.dart';
import 'package:core_elements/core_ajax_dart.dart';
import 'dart:html';

Logger _log = new Logger('ui.polymer.core.staticpage');

@CustomTag('static-page')
class StaticPage extends PolymerElement with Observable {
  @published bool displayed = false;
  @published String contentUrl;
  _TrusingNodeValidator _nodeValidator = new _TrusingNodeValidator();

  CoreAjax _ajax;
  Element _contentContainer;

  StaticPage.created() : super.created();

  void ready() {
    _contentContainer = $['content'];
    _ajax = $['ajax'];
  }

  void handleResponse(e) {
    final String content = e.detail['response'];

    _contentContainer.setInnerHtml(content, validator: _nodeValidator);
  }

  displayedChanged() {
    _log.finest('static-page displayed changed to "$displayed"');
    // to cope with null value for displayed
    if (displayed == true) {
      _ajax.go();
    }
  }
}

class _TrusingNodeValidator implements NodeValidator {
  @override
  bool allowsAttribute(Element element, String attributeName, String value) =>
      true;

  @override
  bool allowsElement(Element element) => true;
}
